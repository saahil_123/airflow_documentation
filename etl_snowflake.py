#import DAG module
from airflow.models import DAG
from datetime import datetime
#since we have to create a table in sqllite database, import  sqllite opeartor.
import json
from airflow.operators.python import PythonOperator
from airflow.operators.python import BranchPythonOperator
from airflow.operators.dummy import DummyOperator
from airflow.sensors.filesystem import FileSensor
from airflow.exceptions import AirflowSensorTimeout
import psycopg2
import pandas as pd
import time
from airflow.models import Variable
from airflow.contrib.operators.snowflake_operator import SnowflakeOperator
from airflow.contrib.hooks.snowflake_hook import SnowflakeHook
from datetime import datetime



current_time = datetime.now().strftime("%m%d%Y_%H%M%S")


def _read_csv():
    commercial_df = pd.read_csv('/home/airflow/airflow/inputfiles/commercial_1.csv',names=['PRODUCT_NAME', 'CUSTOMER_NAME','VAL1','VAL2','VAL3','VAL4','TYPE','CATEGORY','VAL5','car_info'])
    print(commercial_df)
    normalized_df = pd.json_normalize(commercial_df.car_info.apply(json.loads))
    print(normalized_df)
    final_df = commercial_df.drop('car_info',axis=1).join(normalized_df)
    #final_df = final_df.rename(columns={"car": "CAR", "model": "MODEL"})
    final_df.to_csv(f'/home/airflow/airflow/outputfiles/commercial_1_output_{current_time}.csv',index=False,sep='|')

def _failure_callback(context):
    if isinstance(context['exception'], AirflowSensorTimeout):
        print(context)
        print("file was not created and Sensor timed out")


default_args = {
    'start_date': datetime(2021,10,18)
}

snowflake_query = snowflake_query = [
    """use database demo_db""",
    f"""put file:///home/airflow/airflow/outputfiles/* @%COMMERCIAL_1 AUTO_COMPRESS=FALSE;""",
    """COPY INTO COMMERCIAL_1 from @%COMMERCIAL_1 FILE_FORMAT=(TYPE=CSV FIELD_DELIMITER='|',SKIP_HEADER=1);"""
   
]

snowflake_query_load_target =  [
    """use database demo_db""" ,
    """insert into  COMMERCIAL_1_TARGET (
        select PRODUCT_NAME,
        CUSTOMER_NAME,
        VAL1+VAL2+VAL3+VAL4 as TOTAL_PRICE,
        CATEGORY,
        ROUND(VAL5,2),
        car_maker VARCHAR,
        car_model VARCHAR,
        owner_name VARCHAR,
        owner_ph_num
        from COMMERCIAL_1
        where PRODUCT_NAME IS NOT NULL)"""
]


with DAG('etl_snowflake_1',schedule_interval ='@daily',
        default_args = default_args,
        catchup=False
        ) as dag:

        read_csv = PythonOperator(
        task_id = 'read_csv',
        python_callable = _read_csv
        )

        check_output_file_created = FileSensor(
            task_id = 'check_output_file_created',
            poke_interval = 60,
            timeout=60 * 30,
            on_failure_callback=_failure_callback,
            filepath=f'/home/airflow/airflow/outputfiles/'
        )

        load_data_to_landing = SnowflakeOperator(
            task_id="load_data_to_landing",
            sql=snowflake_query ,
            snowflake_conn_id="snowflake_conn"
    )

        transform_and_load_target = SnowflakeOperator(
            task_id="transform_and_load_target",
            sql=snowflake_query_load_target ,
            snowflake_conn_id="snowflake_conn"
    )

read_csv >> check_output_file_created >> load_data_to_landing >> transform_and_load_target