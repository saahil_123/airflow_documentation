#import DAG module
from airflow.models import DAG
from datetime import datetime
#since we have to create a table in sqllite database, import  sqllite opeartor.
import json
from airflow.operators.python import PythonOperator
from airflow.operators.python import BranchPythonOperator
from airflow.operators.dummy import DummyOperator
from airflow.sensors.filesystem import FileSensor
from airflow.exceptions import AirflowSensorTimeout
import psycopg2
import pandas as pd
import time
from airflow.models import Variable


#use the default args dictionary to instantiate all the default args, that will be default/common
# to all the tasks in the DAG, e.g: start_date

default_args = {
    'start_date': datetime(2021,10,18)
}

# read airflow variables
database_connection_variables = Variable.get("database_connection_variables",deserialize_json=True)
host = database_connection_variables['host']
database = database_connection_variables ['database']
user = database_connection_variables['user']
password = database_connection_variables['password']

#read environment variables
env_variable_key_1 = Variable.get("key_1")

def connect_to_db():
    conn = psycopg2.connect(
    host=host,
    database=database,
    user=user,
    password=password)
    return conn

def _crud_operations():
    conn = connect_to_db()
    # create table dummy
    sql_1 = 'create table dummy1(id int, name varchar)'
    sql_2 = "insert into dummy1 (id, name) values (1,'sahil')"
    sql_3 = 'select * from dummy1'
    sql_4 = "update  dummy1 set name = 'abay' where name = 'sahil'"
    sql_5 = "delete from dummy1 where name = 'abay'"
    cur =  conn.cursor()
    cur.execute(sql_1)
    conn.commit()
    cur.execute(sql_2)
    conn.commit()
    cur.execute(sql_3)
    results = cur.fetchone()
    print(results)
    cur.execute(sql_4)
    conn.commit()
    cur.execute(sql_5)
    conn.commit()
    cur.close()
    conn.close()


def _extract_data():
    conn = connect_to_db()
	 sql = "select * from employees"
    employees_df = pd.read_sql(sql,conn)
    return employees_df

def _clean_filter_data():
    employees_df = _extract_data()
    employees_df = employees_df.astype(str)
    employees_df = employees_df.apply(lambda x: x.str.strip())
    employees_df = employees_df.apply(lambda x: x.str.encode('ascii', 'ignore').str.decode('ascii'))
    employees_df = employees_df[employees_df['sal'] > '3000']
    employees_df.to_sql('sample.csv')
    print(env_variable_key_1)
    print(employees_df)


'''def load_data():
    # Opening JSON file
    f = open('/home/airflow/airflow/myfiles/users.json','r')
    # returns JSON object as
    # a dictionary
    data = json.load(f)

    firstname = data['firstName']
    lastname = data['lastName']
    country = data ['country']
    username = data['username']
    password = data['password']
    email = data ['email']

    sqlite_hook = SqliteHook(sqlite_conn_id = 'db_sqlite')
    #rows = [('a','b','c','d','e','f')]
    #sqlite_hook.insert_rows(table='users', rows=rows,replace= True)
    sqlite_hook.get_records('select * from users')
  #sqlite_hook.insert_rows(table='users', rows=rows,replace= True)
    sqlite_hook.get_records('select * from users')'''

def check_bonus_status():
    bonus_paid = 'no'
    if bonus_paid == 'no':
        return 'process_bonus'
    return 'do_nothing'


def _process_bonus():
    print("paid bonus to the user")

def _failure_callback(context):
    if isinstance(context['exception'], AirflowSensorTimeout):
        print(context)
        print("Sensor timed out")

def _parallel_task_1():
    time.sleep(120)

def _parallel_task_2():
    time.sleep(60)

def _parallel_task_3():
    print("done")

with DAG('user_processing',schedule_interval ='@daily',
        default_args = default_args,
        catchup=False
        ) as dag:


    check_file_a = FileSensor(
	task_id='check_file_a',
    poke_interval=60,
    timeout=60 * 30,
    on_failure_callback=_failure_callback,
    filepath='/home/airflow/airflow/myfiles/abc.txt'
    )

    check_file_b = FileSensor(
    task_id='check_file_b',
    poke_interval=60,
    timeout=60 * 30,
    on_failure_callback=_failure_callback,
    filepath='/home/airflow/airflow/myfiles/sample.csv'
    )

    check_file_created = FileSensor(
    task_id='check_file_created',
    poke_interval=60,
    timeout=60 * 30,
    on_failure_callback=_failure_callback,
    filepath='/home/airflow/airflow/myfiles/abd.txt'
    )

    check_bonus_status= BranchPythonOperator(
        task_id = 'check_bonus_status',
        python_callable = check_bonus_status
    )

    process_bonus = PythonOperator(
        task_id = 'process_bonus',
        python_callable = _process_bonus
    )

    parallel_task_1 = PythonOperator(
        task_id = 'parallel_task_1',
        python_callable = _parallel_task_1
    )

    parallel_task_2 = PythonOperator(
        task_id = 'parallel_task_2',
        python_callable = _parallel_task_2
		 task_id = 'parallel_task_2',
        python_callable = _parallel_task_2
    )

    parallel_task_3 = PythonOperator(
        task_id = 'parallel_task_3',
        python_callable = _parallel_task_3
    )

    do_nothing = DummyOperator(
        task_id = 'do_nothing'
    )

    clean_filter_data = PythonOperator(
        task_id ='clean_filter_data',
        python_callable = _clean_filter_data
    )

    crud_operations = PythonOperator(
        task_id = 'crud_operations',
        python_callable = _crud_operations
    )

clean_filter_data >> crud_operations >> check_file_a >> check_file_b >> check_bonus_status >> [process_bonus, do_nothing ]
process_bonus >> parallel_task_1
process_bonus >> parallel_task_2
process_bonus >> parallel_task_3